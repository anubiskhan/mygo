package main

import "fmt"

func main() {
	thing := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for _, number := range thing {
		this := number % 2
		if this == 0 {
			fmt.Printf("%v is even!", number)
		} else {
			fmt.Printf("%v is odd!", number)
		}

	}
}
