package main

import "fmt"

func main() {
	// var colors map[string]string
	// colors := make(map[int]string)

	// var colors init and equal to a map with [string keys]string values
	colors := map[string]string{
		"red":   "#ff0000",
		"blue":  "#0000FF",
		"white": "#ffffff",
	}

	printMap(colors)
}

func printMap(c map[string]string) {
	for k, v := range c {
		fmt.Printf("The color is %v, the hex is %v\n", k, v)
	}
}
