package main // main is reserved and used to create executable, everything else is a reusable package

import "fmt" // double quotes necessary

func main() {
	fmt.Println("More things!") // double quotes necessary
}